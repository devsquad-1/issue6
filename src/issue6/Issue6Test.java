package issue6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Issue6Test {

	Issue6 refIssue6;

	@BeforeEach
	void beforeTest() throws Exception {
		refIssue6 = new Issue6();
	}

	@Test
	void shouldWriteAndReadData() {
		try {
			refIssue6.writeAndReadData();
			Thread.sleep(5500); // Sleep long because unit test runs faster than the actual Issue6, hence need to sleep longer
			assertEquals(2, refIssue6.map.size()); // Test to pass write data
			assertTrue(refIssue6.isRead); // Test to pass read data
		} catch (InterruptedException e) {

			e.printStackTrace(); 
		}
	}
	
	@AfterEach
	void afterTest() throws Exception {
		refIssue6 = null;
	} 
}
