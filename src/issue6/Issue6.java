package issue6;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Issue6 {
	Map<String, BigDecimal> map = new HashMap<>();
	ExecutorService executor = Executors.newFixedThreadPool(2);
	ReadWriteLock lock = new ReentrantReadWriteLock();
	boolean isRead = false;

	public static void main(String[] args) throws InterruptedException {
		Issue6 refIssue6 = new Issue6();
		refIssue6.writeAndReadData(); // Calls line 25
		System.out.println("Size Init " + refIssue6.map.size());
	} 
 
	// Created a method for writeAndRead
	public void writeAndReadData() {
		executor.submit(() -> {

			lock.writeLock().lock(); // Locks the write task
			System.out.println("Writing to map...");

			try {
				try {
					//TimeUnit.SECONDS.sleep(2); // emulating update
				} catch (Exception e) {
					e.printStackTrace();
				}
				// Stores 2 data into the hashmap
				map.put("stock-ABC", BigDecimal.valueOf(12.35));
				map.put("stock-DEF", BigDecimal.valueOf(16.35));
			} finally {
				lock.writeLock().unlock(); // Unlocks the write task
			}

		});
		
		Runnable readTask = () -> {

			lock.readLock().lock(); // Locks the read task
			System.out.println("Reading from map...");

			try {

				for (Map.Entry<String, BigDecimal> entry : map.entrySet()) {
					System.out.println("Values ....." + entry.getKey() + ": " + entry.getValue()); // Iterate through the hash map
				}
				// System.out.println("Size Read " + map.size()); 
				try {
					//TimeUnit.SECONDS.sleep(3); 
					isRead = true; // Sets to true after reading all the data
				} catch (Exception e) {
					e.printStackTrace();
				}
			} finally {
				lock.readLock().unlock(); // Unlocks the read task
			}
		};

		// multiple reads
		executor.submit(readTask);
		executor.submit(readTask);
		executor.shutdown();
	}

}
