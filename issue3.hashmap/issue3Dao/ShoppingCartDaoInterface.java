package issue3Dao;

import issue3Pojo.Product;

public interface ShoppingCartDaoInterface {
	// Method to add product to cart
	void insertProduct(int pid);

	// Method to remove product from cart
	void deleteProduct(int pid);

	// Method to view products in cart
	void viewProduct();
	
	// Method to checkout products in cart
	void checkoutCart();
	
	// Method to clear the cart
	void clearCart();
}
